// Java Program to perform Arithmetic operations
import java.util.*;
class CalcJava
{
    public static void main(String [] args)
    {
        Scanner input = new Scanner(System.in); // To take input from user
        System.out.println("Enter a:");
        int a = input.nextInt(); //Reads and Take integer value from user
        System.out.println("Enter b: ");
        int b = input.nextInt(); //Reads and Take integer value from user
        System.out.println("1.Addition");
        System.out.println("2.Subtraction");
        System.out.println("3.Multiplication");
        System.out.println("4.Division");
        System.out.println("5.Modulus");
        System.out.println("Enter your choice: ");
        int op = input.nextInt(); //Reads and Take integer from user
        if(op == 1)
        System.out.println("Addition of two numbers : " + (a+b)); // Results Addition of two numbers
        else if(op == 2)
        System.out.println("Difference of two numbers : " + (a-b)); // Results Difference of two numbers
        else if(op == 3)
        System.out.println("Product of two numbers : "+ (a*b)); // Results Product of two numbers
        else if(op == 4)
        System.out.println("Quotient of two numbers : "+(a/b)); // Results Quotient of two numbers
        else if(op == 5)
        System.out.println("Remainder of two numbers : "+(a%b)); // Results Remainder of two numbers
        else
        System.out.println("The operator you have choosen is invalid");
    }
}
