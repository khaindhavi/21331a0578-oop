//C++ function to check whether a number is even or odd.
#include<iostream>
using namespace std;
class Even_odd   // A class with class name Even_odd
{
    public:
    void even_odd(int num)  // function definition
    {
        if((num % 2) == 0) // condition for checking even or not
        cout << num << " is an even number." << endl;
        else
        cout << num << " is an odd number." << endl; 
    }
};
int main()
{
    Even_odd obj;
    int num;
    cout << "Enter a number: ";
    cin >> num; // Taking input from the user
    obj.even_odd(num); //calling the method in the class
    return 0;
}

